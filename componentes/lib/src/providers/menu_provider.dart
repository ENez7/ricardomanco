import 'package:flutter/services.dart'
    show rootBundle; // from bukeh import show
import 'dart:convert';

class _MenuProvider {
  List<dynamic> opciones = [];

  _MenuProvider() {
    // _cargarData();
  }

  Future<List<dynamic>> cargarData() async {
    // ASYNC PARA QUE LA INFORMACION SE CARGUE EN LA LISTA
    final resp = await rootBundle.loadString('data/menu_opts.json');
    Map dataMap = json.decode(resp);
    opciones = dataMap['rutas'];

    return opciones;
  }
}

// La clase es privada, solo se expone una instancia de la misma
// O sea, este archivo solo representa un objeto de la clase
final menuProvider = new _MenuProvider();
