import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final opciones = ['1', '2', '3', '4', '5']; // DEBERIAN SER LITERALES

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes Temp'),
      ),
      body: ListView(
        children: _crearItemsCorta(),
      ),
    );
  }

  List<Widget> _crearItems() {
    List<Widget> lista = []; // DART 2.0 = new List() is deprecated

    for (var opc in opciones) {
      final tempWidget = ListTile(
        title: Text(opc),
      );

      lista..add(tempWidget)..add(Divider());
    }

    return lista;
  }

  List<Widget> _crearItemsCorta() {
    return opciones.map((item) {
      return Column(
        children: [
          ListTile(
            title: Text(item + '!'),
            subtitle: Text('Cualquier cosa'),
            leading: Icon(Icons.account_balance_wallet),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {},
          ),
          Divider(),
        ],
      );
    }).toList(); // Esto retorna una lista de ListTile's
  }
}
