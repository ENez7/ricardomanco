import 'package:flutter/material.dart';

import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icon_string_util.dart';

import 'alert_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
        centerTitle: true,
      ),
      body: _lista(),
    );
  }

  Widget _lista() {
    // ESTO ES PARA CREAR LOS TILES CON LA INFORMACION QUE OBTENGA
    return FutureBuilder(
      future: menuProvider.cargarData(), // ESTO SIEMPRE VA ENLAZADO A UN FUTURE
      initialData: [], // ESTO ES OPCIONAL
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        // print('builder');
        // print(snapshot.data);

        return ListView(
          children: _listaItems(snapshot.data, context),
        );
      },
    );
  }

  // CREAR LOS TILES DE LA LISTA
  List<Widget> _listaItems(List<dynamic> data, BuildContext context) {
    final List<Widget> opciones = [];

    data.forEach((opt) {
      final temp = ListTile(
        title: Text(opt['texto']),
        //leading: Icon(Icons.folder_open),
        leading: getIcon(opt['icon']),
        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.blue),
        onTap: () {
          Navigator.pushNamed(context, opt['ruta']);
        },
      );

      opciones.add(temp);
      opciones.add(Divider());
    });

    return opciones;
  }
}
